# README #

## What is this? ##

Wudo allows you to open an elevated command prompt from a normal command window, or run a command as an administrator from a normal command window.

## Why Wudo? ##

The name Wudo is a play on the words Windows and Sudo. In reality, it isn't the same thing as sudo, but I thought the name sounds good.

## How does it work? ##

It's actually a lot simpler than you might imagine, it runs the wudo process as an administrator, then opens a command window and passes any arguments straight to that.

The most important line is `<UACExecutionLevel>RequireAdministrator</UACExecutionLevel>` in the `Wudo.vcxproj` file. This line will force the program to be run as an administrator, which allows it to open the `cmd.exe` process as an administrator as well.

## How do I install it? ##

* Download the program [here (x64)](https://bitbucket.org/olsoldin/wudo/downloads/wudo_x64.zip) (an [x86](https://bitbucket.org/olsoldin/wudo/downloads/wudo_x86.zip) version is also available)
* Place the exe in any folder in the PATH
* Or place it in a new folder and add it to the PATH (I recommend using [Rapid Environment Editor](http://www.rapidee.com/en/about))

## Examples: ##

### Run a command ###

The command `openfiles` requires administrator privileges to run.

![Screenshot 1](scr_1.PNG)

This is how you run the command. When this gets run, a UAC popup window will ask for confirmation. Click **Yes** to run it.

*Notice the `-p`, that is to keep the window open so we can see if the command runs. If we want to immediately return back to the current window, we can leave it off.*

![Screenshot 2](scr_2.PNG)

This is the admin command prompt that opens.

*Notice the line at the end: "Press any key to continue . . . ". This is the effect of the `-p` argument.*

![Screenshot 3](scr_3.PNG)

And when that closes, we return to the original command window.

![Screenshot 4](scr_4.PNG)

### Open a new command window as admin ###

Just typing `wudo` will open a new command window with administrator privileges.

![Screenshot 4](scr_5.PNG)

Here is that window, while it is open, you cannot interact with the original window (closing the admin window returns control in the same way as the previous example).

![Screenshot 4](scr_6.PNG)