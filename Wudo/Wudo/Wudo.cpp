// Wudo.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

std::vector<char*> pauseFlag = { "/p", "-p", "/pause", "--pause" };

bool flagSet(char *arg, std::vector<char*> flag)
{
	for(int i = 0; i < flag.size(); i++){
		if(strcmp(arg, flag[i]) == 0){
			return true;
		}
	}
	return false;
}

int main(int argc, char *argv[])
{
	if(argc < 2){
		// Open an admin cmd if no arguments are passed
		system("cmd.exe");
	}else{		
		// Concat all the arguments into a string (ignoring the pause argument if it exists)
		int i = 1;
		if(flagSet(argv[i], pauseFlag)){
			i++;
		}

		// /c runs the arguments passed to cmd
		std::string commandLineStr = "cmd.exe /c ";

		// i is initialised above
		for(; i < argc; i++){
			commandLineStr.append(std::string(argv[i]).append(" "));
		}

		// Make sure pause is appended if the pause argument is given
		if(flagSet(argv[1], pauseFlag)){
			commandLineStr.append("& pause");
		}

		// Run the command prompt
		system(commandLineStr.c_str());
	}
	return 0;
}